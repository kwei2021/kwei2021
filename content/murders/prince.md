# La soirée du Prince 
Oyez, oyez, brave gens, bienvenue au bal. Le prince d’Altarba organise ce soir un Bal specta-
culaire ! Célébrant l’arrivée dans la région de la Baronne de Théassalie, la soirée s’annonce pro-
metteuse ! Les cuisiniers ont mis les petits plats dans les grands, ménestrels et saltimbanques
se relaient pour satisfaire l’assemblée. On voit que le prince a fait des efforts pour que tous
puissent profiter d’une soirée mémorable !