---
title: Accueil
menu:
  main:
    weight: 1
---
Le K-WEI est un week end d'intégration ludique organisé par le BDL. 
Tu y trouveras entre autres,
Le Ludothon est une semaine d'activités ludique organisé par le BDL. Tu y
trouveras entre autres, des jeux de sociétés, des jeux de rôles, des projections
de films, des jeux d'ambiances, des jeux videos, des murders …

# Planning
{{< figure src="/images/planning.png" title="Planning du K-WEI" >}}

# Activités

## Jeux de sociétés

Pendant toute la durée du K-WEI, viens jouer avec nous à une large séléction
de jeux de sociétés. Rendez vous à la MED () à n'importe quel heure du jour et de la soirée pour trouver des parties.

## Murders
  - [La soirée du Prince](murders/prince), le vendredi à 20h
  - [L'Ecosysteme](murders/ecosysteme), le vendredi à 20h
  - [Eden : une nouvelle ère pour la galaxie](murders/eden), le samedi à 10h
  - [Documents Secrets](murders/documents-secrets), le samedi à 13h
  - [Choix de Perséphone](murders/choix-de-persephone), le samedi à 13h
  - [Mystères à Poudlard](murders/poudlard), le samedi à 14h
  - [Zelda](murders/zelda), le samedi à 15h et le dimanche à 10h
  - [Equipe de Nuit](murders/equipe-de-nuit), le samedi à 20h
  - [Le Sorceleur](murders/sorceleur), le dimanche à 10h
  - [Bordeciel](murders/bordeciel), le dimanche à 10h
  - [Parapluies](murders/parapluies), le dimanche à 10h


## Jeux d'ambiances
  - [Chaîne alimentaire](ambiance/chaine-alimentaire.md), le dimanche à 10h30
  - [Diplomacy](ambiance/diplomacy), le samedi à 20h

## Projection
  - [Flim Mystere](projections/mystere), le samedi après-midi


## Jeux de rôles
  - [My Hero Academia](jdr/my-hero-academia), le samedi à 19h

## Jeux vidéos
  - [Tournoi Smash Bros Ultimate](jeux-video/smash), le samedi à 13h

## Tournoi
  - [Magic](Tournoi/magic), le vendredi à 20h
  - [8 minutes pour un empire](Tournoi/8-minutes-pour-un-empire), ?
  - [TrollBall](Tournoi/trollball), le samedi à 14h
  - [Echecs](Tournoi/echecs), le samedi à 13h

## Autres
  - [Jeu de figurines](Autres/figurines), le vendredi à 20h
  - [Jeu Mystère](Autres/jeu-mystere), le samedi à 10h
  - [Jonglage](Autres/jonglage), le samedi à 15h

