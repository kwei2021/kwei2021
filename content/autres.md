---
title: Autres
menu:
  main:
    weight: 8
---
# Autres
  - [Jeu de figurines](Autres/figurines), le vendredi à 20h
  - [Jeu Mystère](Autres/jeu-mystere), le samedi à 10h
  - [Jonglage](Autres/jonglage), le samedi à 15h