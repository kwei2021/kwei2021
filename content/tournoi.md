---
title: Tournoi
menu:
  main:
    weight: 7
---

# Tournoi
  - [Magic](Tournoi/magic), le vendredi à 20h
  - [8 minutes pour un empire](Tournoi/8-minutes-pour-un-empire), ?
  - [TrollBall](Tournoi/trollball), le samedi à 14h
  - [Echecs](Tournoi/echecs), le samedi à 13h
