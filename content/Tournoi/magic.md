# Magic the Gathering − Draft ou Scellé

Tournoi de Magic en Limité sur la dernière extension Innistrad : Chasse de Minuit. 
Le choix entre faire du draft et du scellé sera fait en fonction des personnes intéressées par l'événement. Chacun gardera les cartes qu'il a déballées. 
L'inscription coûte 9€ en cas de draft et 17.5€ en cas de scellé. 
Joueur tout niveaux bienvenus.


